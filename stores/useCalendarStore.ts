import { defineStore } from "pinia";

interface CalendarEvent {
  id: number;
  title: string;
  start: string;
  end: string;
}

interface State {
  events: CalendarEvent[];
}

export const useCalendarStore = defineStore("useCalendarStore", {
  state: () => ({
    events: [],  
  }),
  actions: {
    addEvent(event: CalendarEvent) {
      this.events.push(event);
    },
    updateEvent(event: CalendarEvent) {
      const index = this.events.findIndex((e) => e.id === event.id);
      if (index !== -1) {
        this.events[index] = event;
      }
    },
    removeEvent(eventId: number) {
      this.events = this.events.filter((e) => e.id !== eventId);
    },
  },
  getters: {
    getEvents: (state) => state.events,
  },
});
